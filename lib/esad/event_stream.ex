defmodule Esad.EventStream do
  @moduledoc """
  Consume a stream of event and do some analytics

  The event stream should manage multiple "logical" streams
  which can be idenitifed by it's source. The source can be
  a unique identifier for a remote endpoint or an internal
  reference.

  Speficially it's required that the input for each individual
  source is processed in order, it should be able to detect the
  below conditions for any given source and should be able to
  produce minimal metrics for

  * Value differs by more than ±40%
  * Value above/below threshold for `n` periods

  For any source that has not received any data for a configurable
  time period it's state should be removed.
  """

  @doc """
  Process the incoming event for a particular source.

  If the source does not exist it should be created automatically
  """
  def input(%Esad.Event{} = _ev) do
    raise RuntimeError, message: "not implemented"
  end


  @doc """
  Retrieve metrics for the given source

  Returns data in the form:

  %StreamState{
    outputs: [....],
    inputs: [...],
    hourly_error_rate: ...
  }

  Should raise an error if metric is not found.
  """
  def metric!(_stream), do: raise RuntimeError, message: "not implemented"


  @doc """
  Stop the given stream
  """
  def stop(_stream), do: raise RuntimeError, message: "not implemeted"
end
