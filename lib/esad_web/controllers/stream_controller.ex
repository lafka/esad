defmodule EsadWeb.StreamController do
  use EsadWeb, :controller

  alias Esad.Event

  def list(conn, _params) do
    render(conn, "index.html", streams: streams())
  end


  def stream(conn, %{"stream" => stream_id}) do
    stream = streams()[stream_id]
    render(conn, "stream.html", stream_id: stream_id, stream: stream)
  end


  defp streams() do
    stat = File.stat!(__ENV__.file)
    gsec = :calendar.datetime_to_gregorian_seconds(stat.ctime)
    datetime = DateTime.from_gregorian_seconds(gsec)
    ev2 = %Event{stream: "example-2", datetime: datetime}

      %{
        "example-1" => %{
            outputs: [],
            inputs: [],
            stats: %{
              hourly_error_rate: 0,
              inputs: 321,
              outputs: 0
            }
          },

        "example-2" => %{
            outputs: [%{ev2|value: "3 errors in a row", tags: ["error"]}],
            inputs: [],
            stats: %{
              hourly_error_rate: 1,
              inputs: 3,
              outputs: 1
            }
          },
        }
  end
end
